import React, { Component } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import CartOrder from '../components/Cart/CartOrder'
import ShopBlock from '../components/Shop/ShopBlock'
// import Additional from '../components/Additional/Additional'
import { Router, Route, Link } from 'react-router-dom'
import shopJson from '../API/shoppingJson'
import addOtherProduct from '../API/addOtherProduct'
import createHistory from 'history/createHashHistory';

const history = createHistory()

class App extends Component {
  constructor() {
    super();
    this.state = {
      cart: [],
      shopJson,
      addOtherProduct,
    };
  }

  // 添加商品進購物車cart陣列
  addToCart = (product, infoObj) => {
    const newAddCart = this.state.cart;
    let { listAmout, listType, listSize } = infoObj;
    console.log("infoObj");
    console.log(infoObj);
    listAmout = Number(listAmout)


    // 檢查陣列是某有重複的物件,有重複indexOf就取得其陣列序,沒有indexOf就取得-1
    let checkCartIndex = newAddCart.map(item => {
      return item.id
    }).indexOf(product.id)


    // 若陣列沒有重複物件則新增一個新物件進去
    // 若直接把product給push進newAddCart,newAddCart的product會和shopJson給bytefence
    if (checkCartIndex === -1) {
      let newObj = Object.assign({}, product)

      // 新增屬性amout並給予數量值
      newObj.amout = listAmout

      if (listType && listSize) {
        newObj.styleType = listType
        newObj.listSize = listSize
      }

      newAddCart.push(newObj);
    } else {
      //有重複物件就直接改該物件的amout值
      console.log("listAmout是"+listAmout)
      newAddCart[checkCartIndex].amout = listAmout;
      newAddCart[checkCartIndex].styleType = listType;
      newAddCart[checkCartIndex].listSize  = listSize;
      console.log("newAddCart是")
      console.log(newAddCart)
    }


    this.setState({
      cart: newAddCart
    });

  }

  // 移除購物車清單特定品項
  removeCartItem = (product) => {
    const newRemoveCart = this.state.cart;

    //檢查陣列是某有重複的物件,有重複indexOf就取得其陣列序,沒有indexOf就取得-1
    let checkCartIndex = newRemoveCart.map(item => {
      return item.id
    }).indexOf(product.id)

    //若陣列沒有重複物件則新增一個新物件進去
    //若直接把product給push進newRemoveCart,newRemoveCart的product會和shopJson給bytefence
    if (checkCartIndex === -1) {
      let newObj = Object.assign({}, product)
      // 屬性amout數量歸0
      newObj.amout = 0
      newObj.styleType = ""
      newObj.listSize = ""
      newRemoveCart.push(newObj);
    } else {
      //有重複物件就直接改該物件的amout值
      newRemoveCart[checkCartIndex].amout = 0;
    }
    this.setState({
      cart: newRemoveCart
    });

  }

  render() {
    return (
      <div className="App">

        <Router history={history}>

          <div>
            <Header cart={this.state.cart} />
            <main role="main">

              <Route exact path="/"
                render={() => {
                  return (
                    <div>
                      <ShopBlock
                        shopJson={this.state.shopJson}
                        addToCart={this.addToCart}
                      />
                    </div>
                  );

                }}
              />

              <Route exact path="/cart"
                render={() => {
                  return (
                    <div>
                      <CartOrder
                        cart={this.state.cart}
                        addToCart={this.addToCart}
                        removeCartItem={this.removeCartItem}
                      />
                      {/* <Additional
                        addOtherProduct={this.state.addOtherProduct}
                        addToCart={this.addToCart}
                      /> */}
                    </div>
                  );

                }}
              />
            </main>
            <Footer />
          </div>

        </Router>
      </div>
    );
  }
}

export default App;


