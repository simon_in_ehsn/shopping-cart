let shoppingJson = [
  {
    id: "111841",
    name: "金牌大賞-平底鍋",
    price: 990,
    img: "https://img.imynest.com/uploads/E/F3/EF3C4FF2BD_300x200.jpeg",
    styleList: [],
    inStock: 7,
  },
  {
    id: "123456",
    name: "歐洲工藝-鑄鐵鍋",
    price: 950,
    img: "https://img.rvcamp.biz/0968/01.jpg",
    styleList: [],
    inStock: 0,
  },
  {
    id: "155684",
    name: "創新傳統-炒鍋",
    price: 750,
    img: "https://images-cn.ssl-images-amazon.com/images/I/31xmrQBB2cL._SX300_QL70_.jpg",
    styleList: [],
    inStock: 18,
  },
  {
    id: "516222",
    name: "特級廚師-平底鍋",
    price: 660,
    img: "https://my-best.tw/wp-content/uploads/2017/09/%E9%87%91%E5%B1%AC%E6%8A%8A%E6%89%8B-300x200.jpg",
    styleList: [
      {
        name: "有鉚釘",
        inStock: 30,
        subStyle: [
          {
            name: "M",
            inStock: 12,
            subStyle: [
              {
                name: "美規",
                inStock: 8,
              },
              {
                name: "英制",
                inStock: 4,
              },
            ]
          },
          {
            name: "L",
            inStock: 18,
            subStyle: [
              {
                name: "美規",
                inStock: 10,
              },
              {
                name: "英制",
                inStock: 8,
              },
            ]
          }
        ]
      },
      {
        name: "無鉚釘",
        subStyle: [
          {
            name: "L",
            inStock: 8
          },
          {
            name: "XL",
            inStock: 6
          }
        ]
      }
    ],
    inStock: 44
  }
  ,
  {
    id: "616227",
    name: "家常小灶-鑄鐵鍋",
    price: 1250,
    img: "https://imgcache.dealmoon.com/fsvrugccache.dealmoon.com/ugc/b0b/b6d/acd/54e659ef4741f64818d86e4.jpg_300_0_13_e507.jpg",
    styleList: [
      {
        name: "含陶瓷",
        subStyle: [
          {
            name: "S",
            inStock: 7
          },
          {
            name: "M",
            inStock: 18
          }
        ]
      },
      {
        name: "全鑄鐵",
        subStyle: [
          {
            name: "M",
            inStock: 12
          },
          {
            name: "L",
            inStock: 20
          }
        ]
      }
    ],
    inStock: 57,
  },
  {
    id: "805967",
    name: "匠心獨具-平底鍋",
    price: 730,
    img: "https://www.qiang100.com/data/upload/ueditor/20180605/5b15feac60188.png_resize_300_200.jpg",
    styleList: [
      {
        name: "鋁",
        subStyle: [
          {
            name: "S",
            inStock: 7
          },
          {
            name: "M",
            inStock: 18
          }
        ]
      },
      {
        name: "304不銹鋼",
        subStyle: [
          {
            name: "M",
            inStock: 12
          },
          {
            name: "L",
            inStock: 20
          }
        ]
      }
    ],
    inStock: 57,
  }
]
export default shoppingJson;
