let  addOtherProduct = [
    {
      id: "452793",
      name: "加價購-吸塵器A",
      price: 760,
      img: "http://www.hsct.com.tw/upload/products/pgroup01B021602113741.jpg",
      quantity: 0,
      styleList: []
    },
    {
      id: "452795",
      name: "加價購-吸塵器B",
      price: 800,
      img: "http://www.hsct.com.tw/upload/products/pgroup01B021602113741.jpg",
      quantity: 170,
      styleList: []
    },
    {
      id: "452797",
      name: "加價購-吸塵器C",
      price: 990,
      img: "http://www.hsct.com.tw/upload/products/pgroup01B021602113741.jpg",
      quantity: 120,
      styleList: []
    },
    {
      id: "452896",
      name: "加價購-吸塵器D",
      price: 630,
      img: "http://www.hsct.com.tw/upload/products/pgroup01B021602113741.jpg",
      quantity: 6,
      styleList: []
    }
  ]
  export default addOtherProduct;