import React from 'react';
import { BrowserRouter as Router, Route,Link } from 'react-router-dom'
const Header = ({cart}) => {

  return (
    <nav>
      <div className="logo">
        <a href='/'>
          React購物車練習
        </a>
      </div>
      <div className="nav-link">
        <ul>
          <li>
            <Link to='/'>商品專區</Link>
          </li>
          <li>
            <Link to='/cart'>購物車({cart.filter(item => item.amout > 0).length})</Link>
          </li>
        </ul>
      </div>
    </nav>


  );
}


export default Header;

