import React from 'react';

const SelectItem = ({ selectedValue, onChange, data }) => {

	return (
		<select
			value={selectedValue}
			onChange={e => onChange(e)}>
			{data.map((item, index) => {
				return <option key={index} value={item}>{item}</option>
			})}
		</select>
	)
}

export default SelectItem;
