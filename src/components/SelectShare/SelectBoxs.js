import React from 'react';
import SelectItem from './SelectItem';
const SelectBoxs = ({ styleList, selectData, typeChange, sizeChange, amoutChange }) => {

	let {
		listAmout: selectedAmount,
		listSize: selectedSize,
		listType: selectedType
	} = selectData;

	// 第一個下拉選單陣列
	const typeData = styleList.reduce((accumlator, currentValue) => {
		accumlator.push(currentValue.name);
		return accumlator;
	}, []);

	// 篩選出 商品資料 與 當前樣式option 對應的資料 以取得尺寸
	selectedType = selectedType || (styleList && styleList[0] && styleList[0].name);
	const selectedStyle = styleList && styleList.find(item => {
		return selectedType === item.name;
	});
	// 第二個下拉選單陣列
	const subStyleList = selectedStyle && selectedStyle.subStyle;
	const sizeData = subStyleList && subStyleList.reduce(function (accumlator, currentValue) {
		accumlator.push(currentValue.name);
		return accumlator;
	}, []);

	// 第三個下拉選單陣列
	selectedSize = selectedSize || (subStyleList && subStyleList[0] && subStyleList[0].name);
	const selectedSubStyle = subStyleList.find(item => {
		return selectedSize === item.name;
	});

	const inStock = selectedSubStyle.inStock
	const amountData = [...Array(inStock < 10 ? inStock : 10)].reduce(function (
		accumlator,
		currentValue,
		currentIndex
	) {
		currentIndex > 0 && accumlator.push(currentIndex + 1);
		return accumlator;
	},
		[1]);
	//selectedAmount = selectedAmount || 1;




	return (
		<div>

			{styleList.length ?
				<SelectItem
					selectedValue={selectedType}
					onChange={e => typeChange(e)}
					data={typeData}
				/> : null
			}


			{/* sizeData ? */}
			<SelectItem
				selectedValue={selectedSize}
				onChange={e => sizeChange(e)}
				data={sizeData}
			/>
			{/* :  */}
			<SelectItem
				selectedValue={selectedAmount}
				onChange={e => amoutChange(e)}
				data={amountData}
			/>
		</div>


	);
}

export default SelectBoxs;

