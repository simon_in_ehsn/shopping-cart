import React, { Component } from 'react';
import SelectBoxs from '../SelectShare/SelectBoxs';

class CartItem extends Component {
	constructor(props) {
		super(props)
		this.state = {
			listAmout: props.cart.amout,
			listType: props.cart.styleType,
			listSize: props.cart.listSize
		};
	}
	typeChange = (e) => {
		const { value } = e.target;
		const { styleList } = this.props.cart;
		console.log("點擊事件:" + value);

		// setState樣式後 也setState該樣式第一筆尺寸
		styleList.forEach(item => {
			if (value === item.name) {
				let infoObj = {
					...this.state,
					listSize: item.subStyle[0].name,
					listType: value,
					listAmout: 1
				};
				this.setState(infoObj, () => {
					this.props.addToCart(this.props.cart, infoObj)
				}
				);

			}
		});

	}

	sizeChange = (e) => {
		const { value } = e.target
		let infoObj = {
			...this.state,
			listSize: value,
			listAmout: 1
		}
		this.setState(infoObj, () => {
			this.props.addToCart(this.props.cart, infoObj)
		});
	}

	amoutChange = (e) => {
		const { value } = e.target;
		let infoObj = {
			...this.state,
			listAmout: value
		}
		this.setState(infoObj, () => {
			this.props.addToCart(this.props.cart, infoObj)
			console.log(this.state.listAmout)
		});
	}


	render() {
		let { cart, removeCartItem } = this.props;

		// 當庫存量不足10 option的數值對應庫存量 否則庫存量大於10就顯示10
		let defaultOption = cart.inStock < 10 ? cart.inStock : 10;
		defaultOption = Array(defaultOption);

		const SelectOption = defaultOption.length ? [...defaultOption].map((val, index) => {
			return <option key={index} value={index + 1}>{index + 1}</option>
		}) : <option value="0">0</option>


		return (
			<ul>
				<li className="title-1">
					<div className="cart-img">
						<img src={cart.img} alt={cart.name} />
					</div>
					<div className="cart-info">
						<p>{cart.name}</p>
					</div>
				</li>

				<li className="title-2">
				<div className="cart-select">
					{cart.styleList.length ?
						<SelectBoxs
							styleList={cart.styleList}
							selectData={this.state}
							typeChange={this.typeChange}
							sizeChange={this.sizeChange}
							amoutChange={this.amoutChange}
						/>
						:
						<select
							value={cart.amout}
							onChange={event => this.amoutChange(event)}
						>
							{SelectOption}
						</select>
					}
					</div>
				</li>

				<li className="title-3">
					<div className="cart-money">
						<p>單價</p><span>${cart.price}</span>
					</div>
				</li>
				<li className="title-4">
					<div className="cart-money">
						<p>小計</p><span>${cart.price * cart.amout}</span>
					</div>
				</li>
				<li className="title-5">
					<div className="cart-deleted">
						<button onClick={() => removeCartItem(cart)}>×</button>
					</div>
				</li>
			</ul>
		)
	}
}
export default CartItem;