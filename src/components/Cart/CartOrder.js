import React from 'react';
import CartItem from './CartItem';

const CartOrder = ({ cart, addToCart, removeCartItem }) => {
	let totalMoney = null; // 購物車商品總價
	let totalProduct = null; // 購物車商品總項

	totalMoney = cart.reduce((previous, current) => previous + (current.price * current.amout), 0);
	totalProduct = cart.filter(item => item.amout > 0).length;
	return (
		<section>
			<div className="row">
				<h2>購物訂單</h2>
				<div className="cart-order">
					<div className="cart-header">
						<h3>已放入購物車</h3>
						<ul>
							<li className="title-1">品項</li>
							<li className="title-2">規格與數量</li>
							<li className="title-3">單價</li>
							<li className="title-4">小計</li>
							<li className="title-5">編輯</li>
						</ul>
					</div>
					<div className="cart-body">

						{cart.filter(item => item.amout > 0).map((item) => {
							console.log(item)
							return <CartItem
								cart={item}
								key={item.id}
								addToCart={addToCart}
								removeCartItem={removeCartItem}
							/>
						})}



					</div>

					<div className="cart-footer">
						<div className="cart-footer-info1">
							<div className="total-amount">
								總計<span>{totalProduct}</span>項商品
          </div>
						</div>
						<div className="cart-footer-info2">
							<button
								onClick={() => {
									console.log("您購買了")
									for (let i = 0; i < cart.length; i++) {
										console.log(cart[i].name + ":" + "\n材質樣式:" + cart[i].styleType + "\n大小尺寸" + cart[i].listSize + "\n數量" + cart[i].amout + "個")
									}
									console.log("\n共計" + totalMoney + "元")
								}
								}>確認結帳</button>
						</div>
						<div className="cart-footer-info3">
							<div className="total-money">
								共 <span>${totalMoney}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>





	)
}

export default CartOrder;
