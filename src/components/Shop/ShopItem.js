import React, { Component } from 'react';
import SelectBoxs from '../SelectShare/SelectBoxs';

class ShopItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listAmout: "1",
      listType: props.product.styleList.length ? ( !this.listType ? props.product.styleList[0].name : this.listType ) : "",
      listSize: props.product.styleList.length ? ( !this.listSize ? props.product.styleList[0].subStyle[0].name : this.listSize ) : ""
    };
  }
  typeChange = (e) => {
    const { value } = e.target;
    const { styleList } = this.props.product;
    console.log("點擊事件:" + value);

    // setState樣式後 也setState該樣式第一筆尺寸
    styleList.forEach(item => {
      if (value === item.name) {
        this.setState({
          ...this.state,
          listSize: item.subStyle[0].name,
          listType: value,
          listAmout: 1
        });
        console.log(item.name)
      }
    });
  }

  sizeChange = (e) => {
    const { value } = e.target
    this.setState({
      ...this.state,
      listSize: value,
      listAmout: 1
    });
    console.log(this.state.listSize);

  }

  amoutChange = (e) => {
    const { value } = e.target;
    this.setState({
      ...this.state,
      listAmout: value
    });
  }


  render() {
    // 當庫存量不足10 option的數值對應庫存量 否則庫存量大於10就顯示10
    const { inStock } = this.props.product;
    let defaultOption = inStock < 10 ? inStock : 10;
    defaultOption = Array(defaultOption);

    const SelectOption = defaultOption.length ? [...defaultOption].map((val, index) => {
      return <option key={index} value={index + 1}>{index + 1}</option>
    }) : <option value="0">0</option>


    return (
      
      <li className="product-item">

        <div className="img-block">
          <img src={this.props.product.img} alt={this.props.product.name} />
        </div>
          <h3>{this.props.product.name}</h3>
          <div className="select-block">
          {this.props.product.styleList.length ?
                <SelectBoxs
                  selectData={this.state}
                  styleList={this.props.product.styleList}
                  typeChange={this.typeChange}
                  sizeChange={this.sizeChange}
                  amoutChange={this.amoutChange}
                />
                :
                // 沒有List樣式時的商品
                
                  <select
                    value={this.state.listAmout}
                    onChange={this.amoutChange}
                  >
                    {SelectOption}
                  </select>
              
              }
          </div>

          <div className="btn-block">
            <h3>${this.props.product.price}</h3>
            <button type="button"
              onClick={() => this.props.addToCart(this.props.product, this.state)}
              disabled={inStock ? '' : 'disabled'}
            >放入購物車</button>
          </div>
      </li>



        )
      }

    }
    export default ShopItem;
    
