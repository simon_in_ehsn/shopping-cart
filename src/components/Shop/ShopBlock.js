import React from 'react';
import ShopItem from './ShopItem'

const ShopZone = ({ shopJson, addToCart }) => {
    return (
			<section>
			<div className="row">
					<h2>商品專區</h2>
					<ul>
							{shopJson.map((item, index) => {
									return <ShopItem
											product={item}
											key={index}
											addToCart={addToCart}
									/>
							})}
					</ul>
			</div>
			</section>
    )
}

export default ShopZone;