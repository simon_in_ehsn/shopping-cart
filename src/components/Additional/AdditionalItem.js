import React, { Component } from 'react';

class AdditionalItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listAmout: "1",
            listType:"",
            listSize:""
        };
    }
    // event捕捉 改變state內定義select的值
    handleChange = (event) => {
        this.setState({ listAmout: event.target.value });
    }
    render() {
        // 當庫存量不足10 option的數值對應庫存量 否則庫存量大於10就顯示10
        const { quantity } = this.props.product;
        let defaultOption = quantity < 10 ? quantity : 10;
        defaultOption = Array(defaultOption);

        const SelectOption = defaultOption.length ? defaultOption.fill().map((val, index) => {
            return <option key={index} value={index + 1}>{index + 1}</option>
        }) : <option value="0">0</option>

        return (
            <div className="col-md-3">
                <div className="card mb-4 box-shadow text-left">
                    <img className="card-img-top" src={this.props.product.img} alt={this.props.product.name} />
                    <div className="card-body">
                        <h4>{this.props.product.name}</h4>
                        <h5>${this.props.product.price}</h5>

                        <div className="row mt-4">
                            <div className="col-sm-12 my-1">
                                <select className="custom-select mr-sm-2"
                                    value={this.state.listAmout}
                                    onChange={this.handleChange}                            >
                                    {SelectOption}
                                </select>
                            </div>
                            <div className="col-sm-12 my-1">
                                <button type="button" className="btn btn-outline-danger add-to-cart"
                                    onClick={() => this.props.addToCart(this.props.product, this.state)}
                                    disabled={this.props.product.quantity ? '' : 'disabled'}
                                >選購</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div >
        )
    }

}
export default AdditionalItem;

